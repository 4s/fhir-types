export = fhir

/** Module containing interfaces for all used FHIR resources in accordance with FHIR version 3.5.0 */
declare namespace fhir {
  /**
   * 	Any combination of upper or lower case ASCII letters ('A'..'Z', and 'a'..'z', numerals ('0'..'9'), '-' and '.', with a length limit of 64 characters. (This might be an integer, an un-prefixed OID, UUID or any other identifier pattern that meets these constraints.)
   */
  type id = string
  /**
   * A Uniform Resource Identifier Reference (RFC 3986 ). Note: URIs are case sensitive.
   */
  type uri = string
  /**
   * A Uniform Resource Locator (http://tools.ietf.org/html/rfc1738)
   */
  type url = string
  /**
   * A URI that refers to a canonical URI.
   */
  type canonical = uri
  /**
   * A stream of bytes, base64 encoded (RFC 4648).
   */
  type base64Binary = string
  /**
   * A string which has at least one character and no leading or trailing whitespace and where there is no whitespace other than single spaces in the contents
   */
  type code = string
  /**
   * A date, or partial date (e.g. just year or year + month) as used in human communication. The format is YYYY, YYYY-MM, or YYYY-MM-DD, e.g. 2018, 1973-06, or 1905-08-23. There SHALL be no time zone.
   */
  type date = string
  /**
   * A date, date-time or partial date (e.g. just year or year + month) as used in human communication. The format is YYYY, YYYY-MM, YYYY-MM-DD or YYYY-MM-DDThh:mm:ss+zz:zz, e.g. 2018, 1973-06, 1905-08-23, 2015-02-07T13:28:17-05:00 or 2017-01-01T00:00:00.000Z. If hours and minutes are specified, a time zone SHALL be populated. Seconds must be provided due to schema type constraints but may be zero-filled and may be ignored at receiver discretion. Dates SHALL be valid dates.
   */
  type dateTime = string
  /**
   * Rational numbers that has a decimal representation.
   */
  type decimal = number
  /**
   * An instant in time in the format YYYY-MM-DDThh:mm:ss.sss+zz:zz. The time SHALL be specified at least to the second and SHALL include a time zone.
   */
  type instant = string
  /**
   * A signed integer in the range −2,147,483,648..2,147,483,647 (32-bit; for larger values, use decimal)
   */
  type integer = number
  /**
   * A FHIR string that may contain markdown syntax for optional processing by a markdown presentation engine, in the GFM extension of CommonMark format
   */
  type markdown = string
  /**
   * An OID represented as a URI (RFC 3001 ); e.g. urn:oid:1.2.3.4.5
   */
  type oid = string
  /**
   * Any positive integer in the range 1..2,147,483,647
   */
  type positiveInt = number
  /**
   * A time during the day, in the format hh:mm:ss. There is no date specified. Seconds must be provided due to schema type constraints but may be zero-filled and may be ignored at receiver discretion.
   */
  type time = string
  /**
   * 	Any non-negative integer in the range 0..2,147,483,647
   */
  type unsignedInt = number
  /**
   * A UUID (aka GUID) represented as a URI (RFC 4122 ); e.g. urn:uuid:c757873d-ec9a-4326-a141-556f43239520
   */
  type uuid = string

  /**
   * An address expressed using postal conventions (as opposed to GPS or other location definition formats)
   */
  interface Address extends Element {
    /**
     * home | work | temp | old - purpose of this address
     */
    use?: AddressUse
    /**
     * postal | physical | both
     */
    type?: AddressType
    /**
     * Text representation of the address
     */
    text?: string
    /**
     * Street name, number, direction & P.O. Box etc.
     */
    line?: string[]
    /**
     * Name of city, town etc.
     */
    city?: string
    /**
     * District name (aka county)
     */
    district?: string
    /**
     * Sub-unit of country (abbreviations ok)
     */
    state?: string
    /**
     * Postal code for area
     */
    postalCode?: string
    /**
     * Country (e.g. can be ISO 3166 2 or 3 letter code)
     */
    country?: string
    /**
     * Time period when address was/is in use
     */
    period?: Period
  }

  /**
   * All possible values for Address.use
   */
  type AddressUse = 'home' | 'work' | 'temp' | 'old'
  /**
   * All possible values for Address.type
   */
  type AddressType = 'postal' | 'physical' | 'both'

  /**
   * A duration of time during which an organism (or a process) has existed
   */
  interface Age extends Quantity {}

  /**
   * Text node with attribution
   */
  interface Annotation extends Element {
    /**
     * Individual responsible for the annotation
     */
    authorReference?: Reference
    /**
     * Individual responsible for the annotation
     */
    authorString?: string
    /**
     * Contains extended information for property 'authorString'.
     */
    time?: dateTime
    /**
     * The annotation  - text content
     */
    text: string
  }

  /**
   * Content in a format defined elsewhere
   */
  interface Attachment extends Element {
    /**
     * Mime type of the content, with charset etc.
     */
    contentType?: code
    /**
     * Human language of the content (BCP-47)
     */
    language?: code
    /**
     * Data inline, base64ed
     */
    data?: base64Binary
    /**
     * Uri where the data can be found
     */
    url?: uri
    /**
     * Number of bytes of content (if url provided)
     */
    size?: unsignedInt
    /**
     * Hash of the data (sha-1, base64ed)
     */
    hash?: base64Binary
    /**
     * Label to display in place of the data
     */
    title?: string
    /**
     * Date attachment was first created
     */
    creation?: dateTime
  }

  /**
   * Base for elements defined inside a resource
   */
  interface BackboneElement extends Element {
    /**
     * Extensions that cannot be ignored if unrecognized
     */
    modifierExtension?: Extension[]
  }

  /**
   * Basic is used for handling concepts not yet defined in FHIR, narrative-only resources that don't map to an existing resource, 
   * and custom resources not appropriate for inclusion in the FHIR specification.
   */
  interface Basic extends DomainResource {
    /**
     * Business identifier
     */
    identifier?: Identifier[]
    /**
     * Kind of Resource (http://hl7.org/fhir/valueset-basic-resource-type.html)
     */
    code: CodeableConcept
    /**
     * Identifies the focus of this resource
     */
    subject?: Reference
    /**
     * When created
     */
    created?: date
    /**
     * Who created
     */
    author?: Reference
  }

  /**
   * Contains a collection of resources
   */
  interface Bundle extends Resource {
    /**
     * Persistent identifier for the bundle
     */
    identifier?: Identifier
    /**
     * document | message | transaction | transaction-response | batch | batch-response | history | searchset | collection
     */
    type: 'document' | 'message' | 'transaction' | 'transaction-response' | 'batch' | 'batch-response' | 'history' | 'searchset' | 'collection'
    /**
     * When the bundle was assembled
     */
    timestamp?: instant
    /**
     * If search, the total number of matches
     */
    total?: unsignedInt
    /**
     * Links related to this Bundle
     */
    link?: BundleLink[]
    /**
     * Entry in the bundle - will have a resource, or information
     */
    entry?: BundleEntry[]
    /**
     * Digital Signature
     */
    signature?: Signature
  }

  /**
   * Entry in the bundle - will have a resource, or information
   */
  interface BundleEntry extends BackboneElement {
    /**
     * Links related to this entry
     */
    link?: BundleLink[]
    /**
     * Absolute URL for resource (server address, or UUID/OID)
     */
    fullUrl?: uri
    /**
     * A resource in the bundle
     */
    resource?: Resource
    /**
     * Search related information
     */
    search?: BundleEntrySearch
    /**
     * Transaction Related Information
     */
    request?: BundleEntryRequest
    /**
     * Transaction Related Information
     */
    response?: BundleEntryResponse
  }

  /**
   * Transaction Related Information
   */
  interface BundleEntryRequest extends BackboneElement {
    /**
     * GET | POST | PUT | DELETE
     */
    method: 'GET' | 'POST' | 'PUT' | 'DELETE'
    /**
     * URL for HTTP equivalent of this entry
     */
    url: uri
    /**
     * For managing cache currency
     */
    ifNoneMatch?: string
    /**
     * For managing update contention
     */
    ifModifiedSince?: instant
    /**
     * For managing update contention
     */
    ifMatch?: string
    /**
     * For conditional creates
     */
    ifNoneExist?: string
  }

  /**
   * Transaction Related Information
   */
  interface BundleEntryResponse extends BackboneElement {
    /**
     * Status response code (text optional)
     */
    status: string
    /**
     * The location, if the operation returns a location
     */
    location?: uri
    /**
     * The etag for the resource (if relevant)
     */
    etag?: string
    /**
     * Server's date time modified
     */
    lastModified?: instant
    /**
     * OperationOutcome with hints and warnings (for batch/transaction)
     */
    outcome?: Resource
  }

  /**
   * Search related information
   */
  interface BundleEntrySearch extends BackboneElement {
    /**
     * match | include | outcome - why this is in the result set
     */
    mode?: 'match' | 'include' | 'outcome'
    /**
     * Search ranking (between 0 and 1)
     */
    score?: decimal
  }

  /**
   * Links related to this Bundle
   */
  interface BundleLink extends BackboneElement {
    /**
     * See http://www.iana.org/assignments/link-relations/link-relations.xhtml#link-relations-1
     */
    relation: string
    /**
     * Reference details for the link
     */
    url: uri
  }

  /**
   * Describes the intention of how one or more practitioners intend to deliver care for a particular patient, 
   * group or community for a period of time, possibly limited to care for a specific condition or set of conditions.
   */
  interface CarePlan extends DomainResource {
    /**
     * 	External Ids for this plan
     */
    identifier?: Identifier[]
    /**
     * Instantiates FHIR protocol or definition
     */
    instantiatesCanonical?: canonical[]
    /**
     * Instantiates external protocol or definition
     */
    instantiatesUri?: uri[]
    /**
     * Fulfills CarePlan
     */
    basedOn?: Reference[]
    /**
     * CarePlan replaced by this CarePlan
     */
    replaces?: Reference[]
    /**
     * Part of referenced CarePlan
     */
    partOf?: Reference[]
    /**
     * draft | active | suspended | completed | entered-in-error | cancelled | unknown 
     * (http://hl7.org/fhir/valueset-request-status.html)
     */
    status: CarePlanStatus
    /**
     * proposal | plan | order | option (http://hl7.org/fhir/valueset-care-plan-intent.html)
     */
    intent: CarePlanIntent
    /**
     * Type of plan (http://hl7.org/fhir/valueset-care-plan-category.html)
     */
    category?: CodeableConcept[]
    /**
     * Human-friendly name for the care plan
     */
    title?: string
    /**
     * Summary of nature of plan
     */
    description?: string
    /**
     * Who the care plan is for
     */
    subject: Reference
    /**
     * Encounter created as part of
     */
    encounter?: Reference
    /**
     * Time period plan covers
     */
    period?: Period
    /**
     * Date record was first recorded
     */
    created?: dateTime
    /**
     * Who is the designated responsible party
     */
    author?: Reference
    /**
     * Who provided the content of the care plan
     */
    contributor?: Reference[]
    /**
     * Who's involved in plan?
     */
    careTeam?: Reference[]
    /**
     * Health issues this plan addresses
     */
    addresses?: Reference[]
    /**
     * Information considered as part of plan
     */
    supportingInfo?: Reference[]
    /**
     * Desired outcome of plan
     */
    goal?: Reference[]
    /**
     * Action to occur as part of plan (Provide a reference or detail, not both)
     */
    activity?: CarePlanActivity[]
  }

  /**
   * All possible values for CarePlan.status
   */
  type CarePlanStatus = 'draft' | 'active' | 'suspended' | 'completed' | 'entered-in-error' | 'cancelled' | 'unknown'

  /**
   * All possible values for CarePlan.intent
   */
  type CarePlanIntent = 'proposal' | 'plan' | 'order' | 'option'

  /**
   * Action to occur as part of care plan
   */
  interface CarePlanActivity extends BackboneElement {
    /**
     * Results of the activity ()
     */
    outcomeCodeableConcept?: CodeableConcept[]
    /**
     * Appointment, Encounter, Procedure, etc.
     */
    outcomeReference?: Reference[]
    /**
     * Comments about the activity status/progress
     */
    progress?: Annotation[]
    /**
     * Activity details defined in specific resource
     */
    reference?: Reference
    /**
     * In-line definition of activity
     */
    detail?: CarePlanActivityDetail
    /**
     * Comments about the plan
     */
    note?: Annotation[]
  }

  /**
   * In-line definition of care plan activity
   */
  interface CarePlanActivityDetail extends BackboneElement {
    /**
     * Kind of resource (http://hl7.org/fhir/valueset-care-plan-activity-kind.html)
     */
    kind?: code
    /**
     * Instantiates FHIR protocol or definition
     */
    instantiatesCanonical?: canonical[]
    /**
     * Instantiates external protocol or definition
     */
    instantiatesUri?: uri[]
    /**
     * Detail type of activity
     */
    code?: CodeableConcept  
    /**
     * Why activity should be done or why activity was prohibited
     */
    reasonCode?: CodeableConcept[]
    /**
     * Why activity is needed
     */
    reasonReference?: Reference[]
    /**
     * Goals this activity relates to
     */
    goal?: Reference[]
    /**
     * not-started | scheduled | in-progress | on-hold | completed | cancelled | stopped | unknown | entered-in-error 
     * (http://hl7.org/fhir/valueset-care-plan-activity-status.html)
     */
    status: 'not-started' | 'scheduled' | 'in-progress' | 'on-hold' | 'completed' | 'cancelled' | 'stopped' | 'unknown' | 'entered-in-error' 
    /**
     * Reason for current status
     */
    statusReason?: CodeableConcept
    /**
     * If true, activity is prohibiting action
     */
    doNotPerform?: boolean
    /**
     * When activity is to occur as Timing resource
     * Only one of scheduledTiming, scheduledPeriod or scheduledString are allowed simultaneously
     */
    scheduledTiming?: Timing
    /**
     * When activity is to occur as Period resource
     * Only one of scheduledTiming, scheduledPeriod or scheduledString are allowed simultaneously
     */
    scheduledPeriod?: Period
    /**
     * When activity is to occur as string
     * Only one of scheduledTiming, scheduledPeriod or scheduledString are allowed simultaneously
     */
    scheduledString?: string
    /**
     * Where it should happen
     */
    location?: Reference
    /**
     * Who will be responsible?
     */
    performer?: Reference[]
    /**
     * What is to be administered/supplied as CodeableConcept
     * Only one of productCodeableConcept or productReference are allowed simultaneously
     */
    productCodeableConcept?: CodeableConcept
    /**
     * What is to be administered/supplied as Reference
     * Only one of productCodeableConcept or productReference are allowed simultaneously
     */
    productReference?: Reference
    /**
     * How much to consume/day?
     */
    dailyAmount?: Quantity
    /**
     * How much to administer/supply/consume
     */
    quantity?: Quantity
    /**
     * Extra info describing activity to perform
     */
    description?: string
  }

  /**
   * Concept - reference to a terminology or just text
   */
  interface CodeableConcept extends Element {
    /**
     * Code defined by a terminology system
     */
    coding?: Coding[]
    /**
     * Plain text representation of the concept
     */
    text?: string
  }

  /**
   * A reference to a code defined by a terminology system
   */
  interface Coding extends Element {
    /**
     * Identity of the terminology system
     */
    system?: uri
    /**
     * Version of the system - if relevant
     */
    version?: string
    /**
     * Symbol in syntax defined by the system
     */
    code?: code
    /**
     * Representation defined by the system
     */
    display?: string
    /**
     * If this coding was chosen directly by the user
     */
    userSelected?: boolean
  }

  /**
   * The ContactDetail structure defines general contact details.
   */
  interface ContactDetail extends Element {
    /**
     * Name of an individual to contact
     */
    name?: string
    /**
     * Contact details for individual or organization
     */
    telecom?: ContactPoint[]
  }

  /**
   * Details of a Technology mediated contact point (phone, fax, email, etc.)
   */
  interface ContactPoint extends Element {
    /**
     * phone | fax | email | pager | url | sms | other
     */
    system?: ContactPointSystem
    /**
     * The actual contact point details
     */
    value?: string
    /**
     * home | work | temp | old | mobile - purpose of this contact point
     */
    use?: ContactPointUse
    /**
     * Specify preferred order of use (1 = highest)
     */
    rank?: positiveInt
    /**
     * Time period when the contact point was/is in use
     */
    period?: Period
  }

  /**
   * All possible values of ContactPoint.use
   */
  export type ContactPointUse = 'home' | 'work' | 'temp' | 'old' | 'mobile'

  /**
   * All possible values of ContactPoint.system
   */
  export type ContactPointSystem = 'phone' | 'fax' | 'email' | 'pager' | 'url' | 'sms' | 'other'

  /**
   * A measured or measurable amount
   */
  interface Count extends Quantity {}

  interface DataRequirement extends Element {
    /**
     * The type of the required data (http://hl7.org/fhir/R4/valueset-all-types.html)
     */
    type: code
    /**
     * The profile of the required data
     */
    profile?: canonical[]
    /**
     * E.g. Patient, Practitioner, RelatedPerson, Organization, Location, Device as CodeableConcept
     * Only one of subjectCodeableConcept or subjectReference are allowed simultaneously
     */
    subjectCodeableConcept?: CodeableConcept
    /**
     * E.g. Patient, Practitioner, RelatedPerson, Organization, Location, Device as Reference
     * Only one of subjectCodeableConcept or subjectReference are allowed simultaneously
     */
    subjectReference?: Reference
    /**
     * Indicates specific structure elements that are referenced by the knowledge module
     */
    mustSupport?: string[]
    /**
     * What codes are expected
     */
    codeFilter?: DataRequirementCodeFilter[]
    /**
     * What dates/date ranges are expected
     */
    dateFilter?: DataRequirementDateFilter[]
    /**
     * Number of results
     */
    limit?: positiveInt
    /**
     * Order of the results
     */
    sort?: DataRequirementSort[]
  }

  /**
   * What codes are expected in a DataRequirement element
   */
  interface DataRequirementCodeFilter extends Element {
    /**
     * A code-valued attribute to filter on
     */
    path?: string
    /**
     * A coded (token) parameter to search on
     */
    searchParam?: string
    /**
     * Valueset for the filter
     */
    valueSet?: canonical
    /**
     * What code is expected
     */
    code?: Coding[]
  }
  
  /**
   * What dates/date ranges are expected in a DataRequirement element
   */
  interface DataRequirementDateFilter extends Element {
    /**
     * A code-valued attribute to filter on
     */
    path?: string
    /**
     * A coded (token) parameter to search on
     */
    searchParam?: string
    /**
     * The value of the filter as a dateTime
     * Only one of valueDateTime, valuePeriod or valueDuration are allowed simultaneously
     */
    valueDateTime?: dateTime
    /**
     * The value of the filter as a Period
     * Only one of valueDateTime, valuePeriod or valueDuration are allowed simultaneously
     */
    valuePeriod?: Period
    /**
     * The value of the filter as a Duration
     * Only one of valueDateTime, valuePeriod or valueDuration are allowed simultaneously
     */
    valueDuration?: Duration
  }

  /**
   * Order of the results in a DataRequirement
   */
  interface DataRequirementSort extends Element {
    /**
     * The name of the attribute to perform the sort
     */
    path: string
    /**
     * ascending | descending (http://hl7.org/fhir/R4/valueset-sort-direction.html)
     */
    direction: 'ascending' | 'descending'
  }

  /**
   * Item used in healthcare
   */
  interface Device extends DomainResource {
    /**
     * Instance identifier
     */
    identifier?: Identifier[]
    /**
     * The reference to the definition for the device
     */
    definition?: Reference
    /**
     * Unique Device Identifier (UDI) Barcode string
     */
    udiCarrier?: DeviceUdi
    /**
     * active | inactive | entered-in-error | unknown
     */
    status?: 'active' | 'inactive' | 'entered-in-error' | 'unknown'
    /**
     * online | paused | standby | offline | not-ready | transduc-discon | hw-discon | off
     */
    statusReason?: CodeableConcept[]
    /**
     * The distinct identification code for a biological product regulated as a device
     */
    distinctIdentificationCode?: string
    /**
     * Name of device manufacturer
     */
    manufacturer?: string
    /**
     * Date when the device was made
     */
    manufactureDate?: dateTime
    /**
     * Date and time of expiry of this device (if applicable)
     */
    expirationDate?: dateTime
    /**
     * Lot number of manufacture
     */
    lotNumber?: string
    /**
     * Serial number assigned by the manufacturer
     */
    serialNumber?: string
    /**
     * The name of the device as given by the manufacturer
     */
    deviceName?: DeviceName[]
    /**
     * The model number for the device
     */
    modelNumber?: string
    /**
     * The part number of the device
     */
    partNumber?: string
    /**
     * What kind of device this is
     */
    type?: CodeableConcept
    /**
     * The capabilities supported on a device, the standards to which the device conforms for a particular purpose, and used for the communication
     */
    specialization?: DeviceSpecialization[]
    /**
     * Version number (i.e. software)
     */
    version?: DeviceVersion[]
    /**
     * The actual configuration settings of a device as it actually operates, e.g., regulation status, time properties
     */
    property?: DeviceProperty[]
    /**
     * Patient to whom Device is affixed
     */
    patient?: Reference
    /**
     * Organization responsible for device
     */
    owner?: Reference
    /**
     * Details for human/organization for support
     */
    contact?: ContactPoint[]
    /**
     * Where the resource is found
     */
    location?: Reference
    /**
     * Network address to contact device
     */
    url?: uri
    /**
     * Device notes and comments
     */
    note?: Annotation[]
    /**
     * Safety Characteristics of Device
     */
    safety?: CodeableConcept[]
    /**
     * The parent device
     */
    parent?: Reference
  }

  /**
   * The characteristics, operational status and capabilities of a medical-related component of a medical device.
   */
  interface DeviceDefinition extends DomainResource {
    /**
     * Instance identifier
     */
    identifier?: Identifier
    /**
     * Unique Device Identifier (UDI) Barcode string
     */
    udiDeviceIdentifier?: DeviceDefinitionUdi
    /**
     * Name of device manufacturer
     */
    manufacturerString?: string
    /**
     * Name of device manufacturer
     */
    manufacturerReference?: Reference
    /**
     * A name given to the device to identify it
     */
    deviceName?: DeviceName
    /**
     * The model number for the device
     */
    modelNumber?: string
    /**
     * What kind of device or device system this is
     */
    type?: CodeableConcept
    /**
     * The capabilities supported on a device, the standards to which the device conforms for a particular purpose, and used for the communication
     */
    specialization?: DeviceSpecialization
    /**
     * The actual design of the device or software version running on the device
     */
    version?: string
    /**
     * Safety characteristics of the device
     */
    safety?: CodeableConcept
    /**
     * Shelf Life and storage information (Should be ProductShelfLife, but that type has not yet been defined by FHIR)
     */
    shelfLifeStorage?: any
    /**
     * Dimensions, color etc. (Should be ProductShelfLife, but that type has not yet been defined by FHIR)
     */
    physicalCharacteristics?: any
    /**
     * Language code for the human-readable text strings produced by the device (all supported)
     */
    languageCode?: CodeableConcept
    /**
     * Device capabilities
     */
    capability?: DeviceDefinitionCapability
    /**
     * The actual configuration settings of a device as it actually operates, e.g., regulation status, time properties
     */
    property?: DeviceProperty
    /**
     * Organization responsible for device
     */
    owner?: Reference
    /**
     * Details for human/organization for support
     */
    contact?: ContactPoint
    /**
     * Network address to contact device
     */
    url?: uri
    /**
     * Access to on-line information
     */
    onlineInformation?: uri
    /**
     * Device notes and comments
     */
    note?: Annotation
    /**
     * The quantity of the device present in the packaging (e.g. the number of devices present in a pack, or the number of devices in the same package of the medicinal product)
     */
    quantity?: Quantity
    /**
     * The parent device it can be part of
     */
    parentDevice?: Reference
    /**
     * A substance used to create the material(s) of which the device is made
     */
    material?: DeviceDefinitionMaterial
  }

  /**
   * Device capabilities
   */
  interface DeviceDefinitionCapability extends BackboneElement {
    /**
     * Type of capability
     */
    type: CodeableConcept
    /**
     * Description of capability
     */
    description?: CodeableConcept
  }
  /**
   * A substance used to create the material(s) of which the device is made
   */
  interface DeviceDefinitionMaterial extends BackboneElement {
    /**
     * The substance
     */
    substance: CodeableConcept
    /**
     * Indicates an alternative material of the device
     */
    alternate?: boolean
    /**
     * Whether the substance is a known or suspected allergen
     */
    allergenicIndicator?: boolean
  }

  /**
   * Unique Device Identifier (UDI) Barcode string
   */
  interface DeviceDefinitionUdi extends BackboneElement {
    /**
     * The identifier that is to be associated with every Device that references this DeviceDefintiion for the issuer and jurisdication porvided in the DeviceDefinition.udiDeviceIdentifier
     */
    deviceIdentifier: string
    /**
     * The organization that assigns the identifier algorithm
     */
    issuer?: uri
    /**
     * The jurisdiction to which the deviceIdentifier applies
     */
    jurisdiction?: uri
  }

  /**
   * The name of the device as given by the manufacturer
   */
  interface DeviceName extends BackboneElement {
    /**
     * The name of the device
     */
    name: string
    /**
     * udi-label-name | user-friendly-name | patient-reported-name | manufacturer-name | model-name | other
     */
    type: 'udi-label-name' | 'user-friendly-name' | 'patient-reported-name' | 'manufacturer-name' | 'model-name' | 'other'
  }

  /**
   * The actual configuration settings of a device as it actually operates, e.g., regulation status, time properties
   */
  interface DeviceProperty extends BackboneElement {
    /**
     * Code that specifies the property DeviceDefinitionPropetyCode (Extensible)
     */
    type?: CodeableConcept
    /**
     * Property value as a quantity
     */
    valueQuantity?: Quantity[]
    /**
     * Property value as a code, e.g., NTP4 (synced to NTP)
     */
    valueCode?: CodeableConcept[]
  }

  /**
   * The capabilities supported on a device, the standards to which the device conforms for a particular purpose, and used for the communication
   */
  interface DeviceSpecialization extends BackboneElement {
    /**
     * The standard that is used to operate and communicate
     */
    systemType: CodeableConcept
    /**
     * The version of the standard that is used to operate and communicate
     */
    version?: string
  }

  /**
   * Unique Device Identifier (UDI) Barcode string
   */
  interface DeviceUdi extends BackboneElement {
    /**
     * Mandatory fixed portion of UDI
     */
    deviceIdentifier?: string
    /**
     * UDI Issuing Organization
     */
    issuer?: uri
    /**
     * Regional UDI authority
     */
    jurisdiction?: uri
    /**
     * UDI Machine Readable Barcode String
     */
    carrierAIDC?: base64Binary
    /**
     * UDI Human Readable Barcode String
     */
    carrierHRF?: string
    /**
     * barcode | rfid | manual +
     */
    entryType?: code
  }

  /**
   * The actual design of the device or software version running on the device
   */
  interface DeviceVersion extends BackboneElement {
    /**
     * The type of the device version
     */
    type?: CodeableConcept
    /**
     * A single component of the device version
     */
    component?: Identifier
    /**
     * The version text
     */
    value: string
  }

  /**
   * A length - a value with a unit that is a physical distance
   */
  interface Distance extends Quantity {}

  /**
   * A resource with narrative, extensions, and contained resources
   */
  interface DomainResource extends Resource {
    /**
     * Text summary of the resource, for human interpretation
     */
    text?: Narrative
    /**
     * Contained, inline Resources
     */
    contained?: Resource[]
    /**
     * Additional Content defined by implementations
     */
    extension?: Extension[]
    /**
     * Extensions that cannot be ignored
     */
    modifierExtension?: Extension[]
  }

  /**
   * A length of time
   */
  interface Duration extends Quantity {}

  /**
   * Base for all elements
   */
  interface Element {
    /**
     * xml:id (or equivalent in JSON)
     */
    id?: string
    /**
     * Additional Content defined by implementations
     */
    extension?: Extension[]
  }

  /**
   * The Expression structure defines an expression that generates a value. 
   * The expression is provided in a specifed language (by mime type)
   */
  interface Expression extends Element {
    /**
     * Natural language description of the condition
     */
    description?: string
    /**
     * Short name assigned to expression for reuse
     */
    name?: id
    /**
     * text/cql | text/fhirpath | application/x-fhir-query | etc.
     * (http://hl7.org/fhir/R4/valueset-expression-language.html)
     */
    language: code
    /**
     * Expression in specified language
     */
    expression?: string
    /**
     * Where the expression is found
     */
    reference?: uri
  }

  /**
   * Optional Extensions Element
   */
  interface Extension extends Element {
    /**
     * identifies the meaning of the extension
     */
    url: uri
    /**
     * Value of extension
     */
    valueBase64Binary?: base64Binary
    /**
     * Value of extension
     */
    valueBoolean?: boolean
    /**
     * Value of extension
     */
    valueCanonical?: canonical
    /**
     * Value of extension
     */
    valueCode?: code
    /**
     * Value of extension
     */
    valueDate?: date
    /**
     * Value of extension
     */
    valueDateTime?: dateTime
    /**
     * Value of extension
     */
    valueDecimal?: decimal
    /**
     * Value of extension
     */
    valueId?: id
    /**
     * Value of extension
     */
    valueInstant?: instant
    /**
     * Value of extension
     */
    valueInteger?: integer
    /**
     * Value of extension
     */
    valueMarkdown?: markdown
    /**
     * Value of extension
     */
    valueOid?: oid
    /**
     * Value of extension
     */
    valuePositiveInt?: positiveInt
    /**
     * Value of extension
     */
    valueString?: string
    /**
     * Value of extension
     */
    valueTime?: time
    /**
     * Value of extension
     */
    valueUnsignedInt?: unsignedInt
    /**
     * Value of extension
     */
    valueUri?: uri
    /**
     * Value of extension
     */
    valueUrl?: uri
    /**
     * Value of extension
     */
    valueUuid?: uuid
    /**
     * Value of extension
     */
    valueAddress?: Address
    /**
     * Value of extension
     */
    valueAge?: Age
    /**
     * Value of extension
     */
    valueAnnotation?: Annotation
    /**
     * Value of extension
     */
    valueAttachment?: Attachment
    /**
     * Value of extension
     */
    valueCodeableConcept?: CodeableConcept
    /**
     * Value of extension
     */
    valueCoding?: Coding
    /**
     * Value of extension
     */
    valueContactPoint?: ContactPoint
    /**
     * Value of extension
     */
    valueCount?: Count
    /**
     * Value of extension
     */
    valueDistance?: Distance
    /**
     * Value of extension
     */
    valueDuration?: Duration
    /**
     * Value of extension
     */
    valueHumanName?: HumanName
    /**
     * Value of extension
     */
    valueIdentifier?: Identifier
    /**
     * Value of extension
     */
    valueMoney?: Money
    /**
     * Value of extension
     */
    valuePeriod?: Period
    /**
     * Value of extension
     */
    valueQuantity?: Quantity
    /**
     * Value of extension
     */
    valueRange?: Range
    /**
     * Value of extension
     */
    valueRatio?: Ratio
    /**
     * Value of extension
     */
    valueReference?: Reference
    /**
     * Value of extension
     */
    valueSampledData?: SampledData
    /**
     * Value of extension
     */
    valueSignature?: Signature
    /**
     * Value of extension
     */
    valueTiming?: Timing
    /**
     * Value of extension
     */
    valueMeta?: Meta
  }

  /**
   * Name of a human - parts and usage
   */
  interface HumanName extends Element {
    /**
     * usual | official | temp | nickname | anonymous | old | maiden
     */
    use?: HumanNameUse
    /**
     * Text representation of the full name
     */
    text?: string
    /**
     * Family name (often called 'Surname')
     */
    family?: string
    /**
     * Given names (not always 'first'). Includes middle names
     */
    given?: string[]
    /**
     * Parts that come before the name
     */
    prefix?: string[]
    /**
     * Parts that come after the name
     */
    suffix?: string[]
    /**
     * Time period when name was/is in use
     */
    period?: Period
  }

  /**
   * All possible values of HumanName.use
   */
  export type HumanNameUse = 'usual' | 'official' | 'temp' | 'nickname' | 'anonymous' | 'old' | 'maiden'

  /**
   * An identifier intended for computation
   */
  interface Identifier extends Element {
    /**
     * usual | official | temp | secondary (If known)
     */
    use?: IdentifierUse
    /**
     * Description of identifier
     */
    type?: CodeableConcept
    /**
     * The namespace for the identifier value
     */
    system?: uri
    /**
     * The value that is unique
     */
    value?: string
    /**
     * Time period when id is/was valid for use
     */
    period?: Period
    /**
     * Organization that issued id (may be just text)
     */
    assigner?: Reference
  }

  /**
   * All possible values of Identifier.use
   */
  type IdentifierUse = 'usual' | 'official' | 'temp' | 'secondary' 

  /**
   * Metadata about a resource
   */
  interface Meta extends Element {
    /**
     * Version specific identifier
     */
    versionId?: id
    /**
     * When the resource version last changed
     */
    lastUpdated?: instant
    /**
     * Identifies where the resource comes from
     */
    source?: uri
    /**
     * Profiles this resource claims to conform to
     */
    profile?: uri[]
    /**
     * Security Labels applied to this resource
     */
    security?: Coding[]
    /**
     * Tags applied to this resource
     */
    tag?: Coding[]
  }

  /**
   * An amount of currency
   */
  interface Money extends Element {
    /**
     * Numerical value (with implicit precision)
     */
    value: decimal
    /**
     * ISO 4217 Currency Code
     */
    currency: code
  }

  /**
   * A human-readable formatted text, including images
   */
  interface Narrative extends Element {
    /**
     * generated | extensions | additional | empty
     */
    status: NarrativeStatus
    /**
     * Limited xhtml content
     */
    div: string
  }

  /**
   * All possible values for Narrative.status
   */
  type NarrativeStatus = 'generated' | 'extensions' | 'additional' | 'empty'

  /**
   * Measurements and simple assertions
   */
  interface Observation extends DomainResource {
    /**
     * Business Identifier for observation
     */
    identifier?: Identifier[]
    /**
     * Fulfills plan, proposal or order
     */
    basedOn?: Reference[]
    /**
     * Part of referenced event
     */
    partOf?: Reference[]
    /**
     * registered | preliminary | final | amended | corrected | cancelled | entered-in-error | unknown
     */
    status: 'registered' | 'preliminary' | 'final' | 'amended' | 'corrected' | 'cancelled' | 'entered-in-error' | 'unknown'
    /**
     * Classification of  type of observation
     */
    category?: CodeableConcept[]
    /**
     * Type of observation (code / type)
     */
    code: CodeableConcept
    /**
     * Who and/or what this is about
     */
    subject?: Reference
    /**
     * 	What the observation is about, when it is not about the subject of record
     */
    focus?: Reference[]
    /**
     * Healthcare event during which this observation is made
     */
    encounter?: Reference
    /**
     * Clinically relevant time/time-period for observation
     */
    effectiveDateTime?: dateTime
    /**
     * Clinically relevant time/time-period for observation
     */
    effectivePeriod?: Period
    /**
     * Clinically relevant time/time-period for observation
     */
    effectiveTiming?: Timing
    /**
     * Clinically relevant time/time-period for observation
     */
    effectiveInstant?: instant
    /**
     * Date/Time this was made available
     */
    issued?: instant
    /**
     * Who is responsible for the observation
     */
    performer?: Reference[]
    /**
     * Actual result
     */
    valueQuantity?: Quantity
    /**
     * Actual result
     */
    valueCodeableConcept?: CodeableConcept
    /**
     * Actual result
     */
    valueString?: string
    /**
     * Actual result
     */
    valueBoolean?: boolean
    /**
     * Actual result
     */
    valueInteger?: integer
    /**
     * Actual result
     */
    valueRange?: Range
    /**
     * Actual result
     */
    valueRatio?: Ratio
    /**
     * Actual result
     */
    valueSampledData?: SampledData
    /**
     * Actual result
     */
    valueTime?: time
    /**
     * Actual result
     */
    valueDateTime?: dateTime
    /**
     * Actual result
     */
    valuePeriod?: Period
    /**
     * Why the result is missing
     */
    dataAbsentReason?: CodeableConcept
    /**
     * High, low, normal, etc.
     */
    interpretation?: CodeableConcept[]
    /**
     * Comments about result
     */
    comment?: string
    /**
     * Observed body part
     */
    bodySite?: CodeableConcept
    /**
     * How it was done
     */
    method?: CodeableConcept
    /**
     * Specimen used for this observation
     */
    specimen?: Reference
    /**
     * (Measurement) Device
     */
    device?: Reference
    /**
     * Provides guide for interpretation
     */
    referenceRange?: ObservationReferenceRange[]
    /**
     * Related resource that belongs to the Observation group
     */
    hasMember?: Reference[]
    /**
     * Related measurements the observation is made from
     */
    derivedFrom?: Reference[]
    /**
     * Component results
     */
    component?: ObservationComponent[]
  }

  /**
   * Measurements and simple assertions
   */
  interface ObservationComponent extends BackboneElement {
    /**
     * Type of observation (code / type)
     */
    code: CodeableConcept
    /**
     * Actual result
     */
    valueQuantity?: Quantity
    /**
     * Actual result
     */
    valueCodeableConcept?: CodeableConcept
    /**
     * Actual result
     */
    valueString?: string
    /**
     * Actual result
     */
    valueBoolean?: boolean
    /**
     * Actual result
     */
    valueInteger?: integer
    /**
     * Actual result
     */
    valueRange?: Range
    /**
     * Actual result
     */
    valueRatio?: Ratio
    /**
     * Actual result
     */
    valueSampledData?: SampledData
    /**
     * Actual result
     */
    valueTime?: time
    /**
     * Actual result
     */
    valueDateTime?: dateTime
    /**
     * Actual result
     */
    valuePeriod?: Period
    /**
     * Why the result is missing
     */
    dataAbsentReason?: CodeableConcept
    /**
     * High, low, normal, etc.
     */
    interpretation?: CodeableConcept
    /**
     * Provides guide for interpretation
     */
    referenceRange?: ObservationReferenceRange[]
  }

  /**
   * Definition of an observation
   */
  interface ObservationDefinition extends DomainResource {
    /**
     * Category of observation
     */
    category?: Coding
    /**
     * Type of observation (code / type)
     */
    code: CodeableConcept
    /**
     * Permitted data type for observation value
     */
    permittedDataType?: Coding[]
    /**
     * Multiple results allowed
     */

    multipleResultsAllowed?: boolean
    /**
     * The method or technique used to perform the observation
     */
    method?: CodeableConcept
    /**
     * Preferred report name
     */
    preferredReportName?: string
    /**
     * Characteristics of quantitative results
     */
    quantitativeDetails?: ObservationDefinitionQuantitativeDetails
    /**
     * Reference range for observation result
     */
    qualifiedInterval?: ObservationDefinitionQualifiedInterval[]
    /**
     * 	Value set of valid coded values for the observation
     */
    validCodedValueSet?: uri
    /**
     * Value set of normal coded values for the observation
     */
    normalCodedValueSet?: uri
    /**
     * Value set of abnormal coded values for the observation
     */
    abnormalCodedValueSet?: uri
    /**
     * Value set of critical coded values for the observation
     */
    criticalCodedValueSet?: uri
  }

  /**
   * Reference range for observation result
   */
  interface ObservationDefinitionQualifiedInterval extends BackboneElement {
    /**
     * The category or type of interval
     */
    category?: CodeableConcept
    /**
     * Low bound of reference range, if relevant
     */
    range?: Range
    /**
     * Reference range qualifier
     */
    type?: CodeableConcept
    /**
     * Reference range population
     */
    appliesTo?: CodeableConcept[]
    /**
     * Applicable age range, if relevant
     */
    age?: Range
    /**
     * Applicable gestational age range, if relevant
     */
    gestationalAge?: Range
    /**
     * Condition associated with the reference range
     */
    condition?: string
  }

  /**
   * Characteristics of quantitative results
   */
  interface ObservationDefinitionQuantitativeDetails extends BackboneElement {
    /**
     * Customary unit for quantitative results
     */
    customaryUnit?: Coding
    /**
     * SI unit for quantitative results
     */
    unit?: Coding
    /**
     * SI to Customary unit conversion factor
     */
    conversionFactor?: decimal
    /**
     * Decimal precision of observation quantitative results
     */
    decimalPrecision?: integer
  }

  /**
   * Provides guide for interpretation
   */
  interface ObservationReferenceRange extends BackboneElement {
    /**
     * Low Range, if relevant
     */
    low?: Quantity
    /**
     * High Range, if relevant
     */
    high?: Quantity
    /**
     * Reference range qualifier
     */
    type?: CodeableConcept
    /**
     * Reference range population
     */
    appliesTo?: CodeableConcept[]
    /**
     * Applicable age range, if relevant
     */
    age?: Range
    /**
     * Text based reference range in an observation
     */
    text?: string
  }

  /**
   * A formally or informally recognized grouping of people or organizations formed
   * for the purpose of achieving some form of collective action.
   * Includes companies, institutions, corporations, departments, community groups,
   * healthcare practice groups, payer/insurer, etc.
   */
  interface Organization extends DomainResource {
    /**
     * Identifies this organization across multiple systems
     */
    identifier?: Identifier[]
    /**
     * Whether the organization's record is still in active use
     */
    active?: boolean
    /**
     * Kind of organization (Example: https://www.hl7.org/fhir/valueset-organization-type.html)
     */
    type?: CodeableConcept[]
    /**
     * Name used for the organization
     */
    name?: string
    /**
     * A list of alternate names that the organization is known as, or was known as in the past
     */
    alias?: string[]
    /**
     * A contact detail for the organization
     * (Rule: The telecom of an organization can never be of use 'home')
     */
    telecom?: ContactPoint[]
    /**
     * An address for the organization
     * (Rule: An address of an organization can never be of use 'home')
     */
    address?: Address[]
    /**
     * The organization of which this organization forms a part
     */
    partOf?: Reference
    /**
     * Contact for the organization for a certain purpose
     */
    contact?: OrganizationContact[]
    /**
     * Technical endpoints providing access to services operated for the organization
     */
    endpoint?: Reference[]
  }

  /**
   * Contact for an organization for a certain purpose
   */
  interface OrganizationContact extends BackboneElement {
    /**
     * The type of contact (https://www.hl7.org/fhir/valueset-contactentity-type.html)
     */
    purpose?: CodeableConcept
    /**
     * A name associated with the contact
     */
    name?: HumanName
    /**
     * Contact details (telephone, email, etc.) for a contact
     */
    telecom?: ContactPoint[]
    /**
     * Visiting or postal addresses for the contact
     */
    address?: Address
  }

  /**
   * Information about an individual or animal receiving health care services
   */
  interface Patient extends DomainResource {
    /**
     * An identifier for this patient
     */
    identifier?: Identifier[]
    /**
     * Whether this patient's record is in active use
     */
    active?: boolean
    /**
     * A name associated with the patient
     */
    name?: HumanName[]
    /**
     * A contact detail for the individual
     */
    telecom?: ContactPoint[]
    /**
     * male | female | other | unknown
     */
    gender?: 'male' | 'female' | 'other' | 'unknown'
    /**
     * The date of birth for the individual
     */
    birthDate?: date
    /**
     * Indicates if the individual is deceased or not
     */
    deceasedBoolean?: boolean
    /**
     * Indicates if the individual is deceased or not
     */
    deceasedDateTime?: dateTime
    /**
     * Addresses for the individual
     */
    address?: Address[]
    /**
     * Marital (civil) status of a patient
     */
    maritalStatus?: CodeableConcept
    /**
     * Whether patient is part of a multiple birth
     */
    multipleBirthBoolean?: boolean
    /**
     * Whether patient is part of a multiple birth
     */
    multipleBirthInteger?: integer
    /**
     * Image of the patient
     */
    photo?: Attachment[]
    /**
     * A contact party (e.g. guardian, partner, friend) for the patient
     */
    contact?: PatientContact[]
    /**
     * A list of Languages which may be used to communicate with the patient about his or her health
     */
    communication?: PatientCommunication[]
    /**
     * Patient's nominated primary care provider
     */
    generalPractitioner?: Reference[]
    /**
     * Organization that is the custodian of the patient record
     */
    managingOrganization?: Reference
    /**
     * Link to another patient resource that concerns the same actual person
     */
    link?: PatientLink[]
  }

  /**
   * A list of Languages which may be used to communicate with the patient about his or her health
   */
  interface PatientCommunication extends BackboneElement {
    /**
     * The language which can be used to communicate with the patient about his or her health
     */
    language: CodeableConcept
    /**
     * Language preference indicator
     */
    preferred?: boolean
  }

  /**
   * A contact party (e.g. guardian, partner, friend) for the patient
   */
  interface PatientContact extends BackboneElement {
    /**
     * The kind of relationship
     */
    relationship?: CodeableConcept[]
    /**
     * A name associated with the contact person
     */
    name?: HumanName
    /**
     * A contact detail for the person
     */
    telecom?: ContactPoint[]
    /**
     * Address for the contact person
     */
    address?: Address
    /**
     * male | female | other | unknown
     */
    gender?: PatientGender
    /**
     * Organization that is associated with the contact
     */
    organization?: Reference
    /**
     * The period during which this contact person or organization is valid to be contacted relating to this patient
     */
    period?: Period
  }

  /**
   * All possible values of Patient.gender
   */
  type PatientGender = 'male' | 'female' | 'other' | 'unknown'

  /**
   * Link to another patient resource that concerns the same actual person
   */
  interface PatientLink extends BackboneElement {
    /**
     * The other patient or related person resource that the link refers to
     */
    other: Reference
    /**
     * replaced-by | replaces | refer | seealso - type of link
     */
    type: 'replaced-by' | 'replaces' | 'refer' | 'seealso'
  }

  /**
   * Time range defined by start and end date/time
   */
  interface Period extends Element {
    /**
     * Starting time with inclusive boundary
     */
    start?: dateTime
    /**
     * End time with inclusive boundary, if not ongoing
     */
    end?: dateTime
  }

  /**
   * This resource allows for the definition of various types of plans as a sharable, consumable, and executable artifact
   */
  interface PlanDefinition extends DomainResource {
    /**
     * Canonical identifier for this plan definition, represented as a URI (globally unique)
     */
    url?: uri
    /**
     * Additional identifier for the plan definition
     */
    identifier?: Identifier[]
    /**
     * Business version of the plan definition
     */
    version?: string
    /**
     * Name for this plan definition (computer friendly)
     */
    name?: string
    /**
     * Name for this plan definition (human friendly)
     */
    title?: string
    /**
     * Subordinate title of the plan definition
     */
    subtitle?: string
    /**
     * order-set | clinical-protocol | eca-rule | workflow-definition
     * (http://hl7.org/fhir/R4/valueset-plan-definition-type.html)
     */
    type?: CodeableConcept
    /**
     * draft | active | retired | unknown
     * (http://hl7.org/fhir/R4/valueset-publication-status.html)
     */
    status: PlanDefinitionStatus
    /**
     * For testing purposes, not real usage
     */
    experimentai?: boolean
    /**
     * Type of individual the plan definition is focused on as CodeableConcept
     * Only one of subjectCodeableConcept or subjectReference are allowed simultaneously
     */
    subjectCodeableConcept?: CodeableConcept
    /**
     * Type of individual the plan definition is focused on as Reference
     * Only one of subjectCodeableConcept or subjectReference are allowed simultaneously
     */
    subjectReference?: Reference
    /**
     * Date last changed
     */
    date?: dateTime
    /**
     * Name of the publisher (organization or individual)
     */
    publisher?: string
    /**
     * Contact details for the publisher
     */
    contact?: ContactDetail[]
    /**
     * Natural language description of the plan definition
     */
    description?: markdown
    /**
     * The context that the content is intended to support
     */
    useContext?: UsageContext[]
    /**
     * Intended jurisdiction for plan definition (if applicable)
     * (http://hl7.org/fhir/R4/valueset-jurisdiction.html)
     */
    jurisdiction?: CodeableConcept[]
    /**
     * Why this plan definition is defined
     */
    purpose?: markdown
    /**
     * Describes the clinical usage of the plan
     */
    usage?: string
    /**
     * Use and/or publishing restrictions
     */
    copyright?: markdown
    /**
     * When the plan definition was approved by publisher
     */
    approvalDate?: date
    /**
     * When the plan definition was last reviewed
     */
    lastReviewDate?: date
    /**
     * When the plan definition is expected to be used
     */
    effectivePeriod?: Period
    /**
     * E.g. Education, Treatment, Assessment
     * (http://hl7.org/fhir/R4/valueset-definition-topic.html)
     */
    topic?: CodeableConcept[]
    /**
     * Who authored the content
     */
    author?: ContactDetail[]
    /**
     * Who edited the content
     */
    editor?: ContactDetail[]
    /**
     * Who reviewed the content
     */
    reviewer?: ContactDetail[]
    /**
     * Who endorsed the content
     */
    endorser?: ContactDetail[]
    /**
     * Additional documentation, citations
     */
    relatedArtifact?: RelatedArtifact[]
    /**
     * Logic used by the plan definition
     */
    library?: canonical[]
    /**
     * What the plan is trying to accomplish
     */
    goal?: PlanDefinitionGoal[]
    /**
     * Action defined by the plan
     */
    action?: PlanDefinitionAction[]
  }

  /**
   * All possible values for PlanDefinition.status
   */
  type PlanDefinitionStatus = 'draft' | 'active' | 'retired' | 'unknown'

  /**
   * Action defined by a plan definition
   */
  interface PlanDefinitionAction extends BackboneElement {
    /**
     * User-visible prefix for the action (e.g. 1. or A.)
     */
    prefix?: string
    /**
     * User-visible title
     */
    title?: string
    /**
     * Brief description of the action
     */
    description?: string
    /**
     * Static text equivalent of the action, used if the dynamic aspects cannot be interpreted by the receiving system
     */
    textEquivalent?: string
    /**
     * routine | urgent | asap | stat (http://hl7.org/fhir/R4/valueset-request-priority.html)
     */
    priority?: 'routine' | 'urgent' | 'asap' | 'stat'
    /**
     * Code representing the meaning of the action or sub-actions
     */
    code?: CodeableConcept[]
    /**
     * Why the action should be performed
     */
    reason?: CodeableConcept[]
    /**
     * Supporting documentation for the intended performer of the action
     */
    documentation?: RelatedArtifact[]
    /**
     * What goals this action supports
     */
    goalId?: id[]
    /**
     * Type of individual the action is focused on as CodeableConcept
     * Only one of subjectCodeableConcept or subjectReference are allowed simultaneously
     */
    subjectCodeableConcept?: CodeableConcept
    /**
     * Type of individual the action is focused on as Reference
     * Only one of subjectCodeableConcept or subjectReference are allowed simultaneously
     */
    subjectReference?: Reference
    /**
     * When the action should be triggered
     */
    trigger?: TriggerDefinition[]
    /**
     * Whether or not the action is applicable
     */
    condition?: PlanDefinitionActionCondition[]
    /**
     * Input data requirements
     */
    input?: DataRequirement[]
    /**
     * Output data definition
     */
    output?: DataRequirement[]
    /**
     * Relationship to another action
     */
    relatedAction?: PlanDefinitionActionRelatedAction[]
    /**
     * When the action should take place as dateTime
     * Only one of timingDateTime, timingAge, timingPeriod, timingDuration, timingRange or timingTiming are allowed simultaneously
     */
    timingDateTime?: dateTime
    /**
     * When the action should take place as Age
     * Only one of timingDateTime, timingAge, timingPeriod, timingDuration, timingRange or timingTiming are allowed simultaneously
     */
    timingAge?: Age
    /**
     * When the action should take place as Period
     * Only one of timingDateTime, timingAge, timingPeriod, timingDuration, timingRange or timingTiming are allowed simultaneously
     */
    timingPeriod?: Period
    /**
     * When the action should take place as Duration
     * Only one of timingDateTime, timingAge, timingPeriod, timingDuration, timingRange or timingTiming are allowed simultaneously
     */
    timingDuration?: Duration
    /**
     * When the action should take place as Range
     * Only one of timingDateTime, timingAge, timingPeriod, timingDuration, timingRange or timingTiming are allowed simultaneously
     */
    timingRange?: Range
    /**
     * When the action should take place as Timing
     * Only one of timingDateTime, timingAge, timingPeriod, timingDuration, timingRange or timingTiming are allowed simultaneously
     */
    timingTiming?: Timing
    /**
     * Who should participate in the action
     */
    participant?: PlanDefinitionActionParticipant[]
    /**
     * create | update | remove | fire-event (http://hl7.org/fhir/R4/valueset-action-type.html)
     */
    type?: CodeableConcept
    /**
     * visual-group | logical-group | sentence-group (http://hl7.org/fhir/R4/valueset-action-grouping-behavior.html)
     */
    groupingBehavior?: 'visual-group' | 'logical-group' | 'sentence-group'
    /**
     * any | all | all-or-none | exactly-one | at-most-one | one-or-more (http://hl7.org/fhir/R4/valueset-action-selection-behavior.html)
     */
    selectionBehavior?: 'any' | 'all' | 'all-or-none' | 'exactly-one' | 'at-most-one' | 'one-or-more'
    /**
     * must | could | must-unless-documented (http://hl7.org/fhir/R4/valueset-action-required-behavior.html)
     */
    requiredBehavior?: 'must' | 'could' | 'must-unless-documented'
    /**
     * yes | no (http://hl7.org/fhir/R4/valueset-action-precheck-behavior.html)
     */
    precheckBehavior?: 'yes' | 'no'
    /**
     * single | multiple (http://hl7.org/fhir/R4/valueset-action-cardinality-behavior.html)
     */
    cardinalityBehavior?: 'single' | 'multiple'
    /**
     * Description of the activity to be performed as a canonical
     * Only one of definitionCanonical or definitionUri are allowed simultaneously
     */
    definitionCanonical?: canonical
    /**
     * Description of the activity to be performed as a uri
     * Only one of definitionCanonical or definitionUri are allowed simultaneously
     */
    definitionUri?: uri
    /**
     * Transform to apply the template
     */
    transform?: canonical
    /**
     * Dynamic aspects of the definition
     */
    dynamicValue?: PlanDefinitionActionDynamicValue[]
    /**
     * A sub-action
     */
    action?: PlanDefinitionAction
  }

  /**
   * Whether or not an action in a plan definition is applicable
   */
  interface PlanDefinitionActionCondition extends BackboneElement {
    /**
     * applicability | start | stop (http://hl7.org/fhir/R4/valueset-action-condition-kind.html)
     */
    kind: 'applicability' | 'start' | 'stop'
    /**
     * Boolean-valued expression
     */
    expression?: Expression
  }

  /**
   * Dynamic aspects of the definition of an action in a plan definition
   */
  interface PlanDefinitionActionDynamicValue extends BackboneElement {
    /**
     * The path to the element to be set dynamically
     */
    path?: string
    /**
     * An expression that provides the dynamic value for the customization
     */
    expression?: Expression
  }

  /**
   * Who should participate in the action in a plan definition
   */
  interface PlanDefinitionActionParticipant extends BackboneElement {
    /**
     * patient | practitioner | related-person | device (http://hl7.org/fhir/R4/valueset-action-participant-type.html)
     */
    type: 'patient' | 'practitioner' | 'related-person' | 'device'
    /**
     * E.g. Nurse, Surgeon, Parent (http://hl7.org/fhir/R4/valueset-action-participant-role.html (Example))
     */
    role?: CodeableConcept 
  }

  /**
   * Relationship to another action in a plandefinition
   */
  interface PlanDefinitionActionRelatedAction extends BackboneElement {
    /**
     * What action is this related to
     */
    actionId: id
    /**
     * before-start | before | before-end | concurrent-with-start | concurrent | concurrent-with-end | after-start | after | after-end
     * (http://hl7.org/fhir/R4/valueset-action-relationship-type.html)
     */
    relationship: 'before-start' | 'before' | 'before-end' | 'concurrent-with-start' | 'concurrent' | 'concurrent-with-end' | 'after-start' | 'after' | 'after-end'
    /**
     * Time offset for the relationship as Duration
     * Only one of offsetDuration or offsetRange are allowed simultaneously
     */
    offsetDuration?: Duration
    /**
     * Time offset for the relationship as Range
     * Only one of offsetDuration or offsetRange are allowed simultaneously
     */
    offsetRange?: Range
  }

  /** 
   * What a plan definition is trying to accomplish
   */
  interface PlanDefinitionGoal extends BackboneElement {
    /**
     * E.g. Treatment, dietary, behavioral (http://hl7.org/fhir/R4/valueset-goal-category.html)
     */
    category?: CodeableConcept
    /**
     * Code or text describing the goal
     */
    description: CodeableConcept
    /**
     * high-priority | medium-priority | low-priority (http://hl7.org/fhir/R4/valueset-goal-priority.html)
     */
    priority?: CodeableConcept
    /**
     * When goal pursuit begins (http://hl7.org/fhir/R4/valueset-goal-start-event.html (Example))
     */
    start?: CodeableConcept
    /**
     * What does the goal address (http://hl7.org/fhir/R4/valueset-condition-code.html (Example))
     */
    addresses?: CodeableConcept[]
    /**
     * Supporting documentation for the goal
     */
    documentation?: RelatedArtifact[]
    /**
     * Target outcome for the goal
     */
    target?: PlanDefinitionGoalTarget[]
  }

  /**
   * Target outcome for the goal of a PlanDefinition
   */
  interface PlanDefinitionGoalTarget extends BackboneElement {
    /**
     * The parameter whose value is to be tracked (http://hl7.org/fhir/R4/valueset-observation-codes.html (Example))
     */
    measure?: CodeableConcept
    /**
     * The target value to be achieved as Quantity
     * Only one of detailQuantity, detailRange or detailCodeableConcept are allowed simultaneously
     */
    detailQuantity?: Quantity
    /**
     * The target value to be achieved as Range
     * Only one of detailQuantity, detailRange or detailCodeableConcept are allowed simultaneously
     */
    detailRange?: Range
    /**
     * The target value to be achieved as CodeableConcept
     * Only one of detailQuantity, detailRange or detailCodeableConcept are allowed simultaneously
     */
    detailCodeableConcept?: CodeableConcept
    /**
     * Reach goal within
     */
    due?: Duration
  }

  /**
   * A person who is directly or indirectly involved in the provisioning of healthcare.
   */
  interface Practitioner extends DomainResource {
    /**
     * An identifier for the person as this agent
     */
    identifier?: Identifier[]
    /**
     * Whether this practitioner's record is in active use
     */
    active?: boolean
    /**
     * The name(s) associated with the practitioner
     */
    name?: HumanName[]
    /**
     * A contact detail for the practitioner (that apply to all roles)
     */
    telecom?: ContactPoint[]
    /**
     * Address(es) of the practitioner that are not role specific (typically home address)
     */
    address?: Address[]
    /**
     * male | female | other | unknown (https://www.hl7.org/fhir/valueset-administrative-gender.html)
     */
    gender?: code
    /**
     * The date on which the practitioner was born
     */
    birthDate?: date
    /**
     * Image of the person
     */
    photo?: Attachment[]
    /**
     * Certification, licenses, or training pertaining to the provision of care
     */
    qualification?: PractitionerQualification[]
    /**
     * A language the practitioner can use in patient communication (https://www.hl7.org/fhir/valueset-languages.html)
     */
    communication?: CodeableConcept[]
  }

  /**
   * Certification, licenses, or training pertaining to the provision of care of a Practitioner
   */
  interface PractitionerQualification extends BackboneElement {
    /**
     * An identifier for this qualification for the practitioner
     */
    identifier?: Identifier[]
    /**
     * Coded representation of the qualification (https://www.hl7.org/fhir/v2/0360/2.7/index.html)
     */
    code: CodeableConcept
    /**
     * Period during which the qualification is valid
     */
    period?: Period
    /**
     * Organization that regulates and issues the qualification
     */
    issuer?: Reference
  }

  /**
   * A measured or measurable amount
   */
  interface Quantity extends Element {
    /**
     * Numerical value (with implicit precision)
     */
    value?: decimal
    /**
     * < | <= | >= | > - how to understand the value
     */
    comparator?: QuantityComparator
    /**
     * Unit representation
     */
    unit?: string
    /**
     * System that defines coded unit form
     */
    system?: uri
    /**
     * Coded form of the unit
     */
    code?: code
  }

  /**
   * All possible values for Quantity.Status
   */
  type QuantityComparator = '<' | '<=' | '>=' | '>'

  /**
   * A structured set of questions intended to guide the collection of answers from end-users
   */
  interface Questionnaire extends DomainResource {
    /**
     * Canonical identifier for this questionnaire, represented as a URI (globally unique)
     */
    url?: uri
    /**
     * Additional identifier for the questionnaire
     */
    identifier?: Identifier[]
    /**
     * Business version of the questionnaire
     */
    version?: string
    /**
     * Name for this questionnaire (computer friendly)
     */
    name?: string
    /**
     * Name for this questionnaire (human friendly)
     */
    title?: string
    /**
     * Instantiates protocol or definition
     */
    derivedFrom?: canonical
    /**
     * The status of this questionnaire. (draft | active | retired | unknown)
     */  
    status: QuestionnaireStatus
    /**
     * For testing purposes, and not real usage
     */
    experimental?: boolean
    /**
     * Resource that can be subject of QuestionnaireResponse
     */
    subjectType?: code[]
    /**
     * Date last changed
     */
    date?: dateTime
    /**
     * Name of the publisher (organization or individual)
     */ 
    publisher?: string
    /**
     * Contact details for the publisher
     */
    contact?: markdown[]
    /**
     * Natural language description of the questionnaire
     */
    description?: markdown
    /**
     * The context that the content is intended to support
     */
    useContext?: UsageContext[]
    /**
     * Intended jurisdiction for questionnaire (if applicable)
     */
    jurisdiction?: CodeableConcept[]
    /**
     * Why this questionnaire is defined
     */
    purpose?: markdown
    /**
     * Use and/or publishing restrictions
     */
    copyright?: markdown
    /**
     * When the questionnaire was approved by publisher
     */
    approvalDate?: date
    /**
     * When the questionnaire was last reviewed
     */
    lastReviewDate?: date
    /**
     * When the questionnaire is expected to be used
     */
    effectivePeriod?: Period
    /**
     * Concept that represents the overall questionnaire
     */
    code?: Coding[]
    /**
     * Questions and sections within the Questionnaire
     */
    item?: QuestionnaireItem[]
  }

  type QuestionnaireStatus = 'draft' | 'active' | 'retired' | 'unknown'

  /**
   * Questions and sections within a Questionnaire
   */
  interface QuestionnaireItem extends BackboneElement {
    /**
     * Unique id for item in questionnaire
     */
    linkId: string
    /**
     * ElementDefinition - details for the item
     */
    definition?: uri
    /**
     * Corresponding concept for this item in a terminology
     */
    code?: Coding[]
    /**
     * A short label for a particular group, question or set of display text within the questionnaire.
     */
    prefix?: string
    /**
     * Primary text for the item
     */
    text?: string
    /**
     * The type of questionnaire item this is. (group | display | boolean | decimal | integer | date | dateTime +)
     */
    type: code
    /**
     * Only allow data when
     */
    enableWhen?: QuestionnaireItemEnableWhen[]
    /**
     *  Controls how multiple enableWhen values are interpreted - whether all or any must be true. (all | any)
     */
    enableBehavior?: QuestionnaireItemEnableBehavior
    /**
     * Whether the item must be included in data results
     */
    required?: boolean
    /**
     * Whether the item may repeat
     */
    repeats?: boolean
    /**
     * Don't allow human editing
     */
    readOnly?: boolean
    /**
     * No more than this many characters
     */
    maxLength?: integer
    /**
     * Valueset containing permitted answers
     */
    answerValueSet?: canonical
    /**
     * Permitted answer
     */
    answerOption?: QuestionnaireItemAnswerOption[]
    /**
     * Initial value(s) when item is first rendered
     */
    initial?: QuestionnaireItemInitial[]
    /**
     * Nested questionnaire items
     */
    item?: QuestionnaireItem[]
  }
  
  type QuestionnaireItemEnableBehavior = 'all' | 'any'

  /**
   * Permitted answer to a QuestionnaireItem
   */
  interface QuestionnaireItemAnswerOption extends BackboneElement {
    /**
     * Answer value in the form of an integer
     */
    valueInteger?: integer
    /**
     * Answer value in the form of a date
     */
    valueDate?: date
    /**
     * Answer value in the form of a time
     */
    valueTime?: time
    /**
     * Answer value in the form of a string
     */
    valueString?: string
    /**
     * Answer value in the form of a Coding
     */
    valueCoding?: Coding
    /**
     * Answer value in the form of a Reference
     */
    valueReference?: Reference
    /**
     * Whether option is selected by default
     */
    initialSelected?: boolean
  }

  /**
   * An object that specifies conditions, under which data should be allowed in a QuestionnaireItem
   */
  interface QuestionnaireItemEnableWhen extends BackboneElement {
    /**
     * Question that determines whether item is enabled
     */
    question: string
    /**
     * Specifies the criteria by which the question is enabled.
     */
    operator: code
    /**
     * Value for question comparison based on operator in the form of a boolean
     */
    answerBoolean?: boolean
    /**
     * Value for question comparison based on operator in the form of a decimal
     */
    answerDecimal?: decimal
    /**
     * Value for question comparison based on operator in the form of an integer
     */
    answerInteger?: integer
    /**
     * Value for question comparison based on operator in the form of a date
     */
    answerDate?: date
    /**
     * Value for question comparison based on operator in the form of a dateTime
     */
    answerDateTime?: dateTime
    /**
     * Value for question comparison based on operator in the form of a time
     */
    answerTime?: time
    /**
     * Value for question comparison based on operator in the form of a string
     */
    answerString?: string
    /**
     * Value for question comparison based on operator in the form of a Coding
     */
    answerCoding?: Coding
    /**
     * Value for question comparison based on operator in the form of a Quantity
     */
    answerQuantity?: Quantity
    /**
     * Value for question comparison based on operator in the form of a Reference
     */
    answerReference?: Reference
  }

  /**
   * Initial value(s) for QuestionnaireItem when item is first rendered
   */
  interface QuestionnaireItemInitial extends BackboneElement {
    /**
     * Actual value for initializing the question in the form of a boolean
     */
    valueBoolean?: boolean
    /**
     * Actual value for initializing the question in the form of a decimal
     */
    valueDecimal?: decimal
    /**
     * Actual value for initializing the question in the form of an integer
     */
    valueInteger?: integer
    /**
     * Actual value for initializing the question in the form of a date
     */
    valueDate?: date
    /**
     * Actual value for initializing the question in the form of a dateTime
     */
    valueDateTime?: dateTime
    /**
     * Actual value for initializing the question in the form of a time
     */
    valueTime?: time
    /**
     * Actual value for initializing the question in the form of a string
     */
    valueString?: string
    /**
     * Actual value for initializing the question in the form of a uri
     */
    valueUri?: uri
    /**
     * Actual value for initializing the question in the form of an Attachment
     */
    valueAttachment?: Attachment
    /**
     * Actual value for initializing the question in the form of a Coding
     */
    valueCoding?: Coding
    /**
     * Actual value for initializing the question in the form of a Quantity
     */
    valueQuantity?: Quantity
    /**
     * Actual value for initializing the question in the form of a Reference
     */
    valueReference?: Reference
  }

  /**
   * A structured set of questions and their answers
   */
  interface QuestionnaireResponse extends DomainResource {
    /**
     * Unique id for this set of answers
     */
    identifier?: Identifier
    /**
     * Request fulfilled by this QuestionnaireResponse
     */
    basedOn?: Reference[]
    /**
     * Part of this action
     */
    partOf?: Reference[]
    /**
     * Form being answered
     */
    questionnaire?: canonical
    /**
     * in-progress | completed | amended | entered-in-error | stopped
     */
    status: 'in-progress' | 'completed' | 'amended' | 'entered-in-error' | 'stopped'
    /**
     * The subject of the questions
     */
    subject?: Reference
    /**
     * Encounter or Episode during which questionnaire was completed
     */
    context?: Reference
    /**
     * Date the answers were gathered
     */
    authored?: dateTime
    /**
     * Person who received and recorded the answers
     */
    author?: Reference
    /**
     * The person who answered the questions
     */
    source?: Reference
    /**
     * Groups and questions
     */
    item?: QuestionnaireResponseItem[]
  }

  /**
   * Groups and questions
   */
  interface QuestionnaireResponseItem extends BackboneElement {
    /**
     * Pointer to specific item from Questionnaire
     */
    linkId: string
    /**
     * ElementDefinition - details for the item
     */
    definition?: uri
    /**
     * Name for group or question text
     */
    text?: string
    /**
     * The response(s) to the question
     */
    answer?: QuestionnaireResponseItemAnswer[]
    /**
     * Nested questionnaire response items
     */
    item?: QuestionnaireResponseItem[]
  }

  /**
   * The response(s) to the question
   */
  interface QuestionnaireResponseItemAnswer extends BackboneElement {
    /**
     * Single-valued answer to the question
     */
    valueBoolean?: boolean
    /**
     * Single-valued answer to the question
     */
    valueDecimal?: decimal
    /**
     * Single-valued answer to the question
     */
    valueInteger?: integer
    /**
     * Single-valued answer to the question
     */
    valueDate?: date
    /**
     * Single-valued answer to the question
     */
    valueDateTime?: dateTime
    /**
     * Single-valued answer to the question
     */
    valueTime?: time
    /**
     * Single-valued answer to the question
     */
    valueString?: string
    /**
     * Single-valued answer to the question
     */
    valueUri?: uri
    /**
     * Single-valued answer to the question
     */
    valueAttachment?: Attachment
    /**
     * Single-valued answer to the question
     */
    valueCoding?: Coding
    /**
     * Single-valued answer to the question
     */
    valueQuantity?: Quantity
    /**
     * Single-valued answer to the question
     */
    valueReference?: Reference
    /**
     * Nested groups and questions
     */
    item?: QuestionnaireResponseItem[]
  }

  /**
   * A set of ordered Quantity values defined by a low and high limit.
   */
  interface Range extends Element {
    /**
     * Low limit
     */
    low: Quantity
    /**
     * High limit
     */
    high: Quantity
  }

  /**
   * A ratio of two Quantity values - a numerator and a denominator
   */
  interface Ratio extends Element {
    /**
     * Numerator value
     */
    numerator?: Quantity
    /**
     * Denominator value
     */
    denominator?: Quantity
  }

  /**
   * A reference from one resource to another
   */
  interface Reference extends Element {
    /**
     * Literal reference, Relative, internal or absolute URL
     */
    reference?: string
    /**
     * 	Type the reference refers to (e.g. "Patient")
     */
    type?: uri
    /**
     * Logical reference, when literal reference is not known
     */
    identifier?: Identifier
    /**
     * Text alternative for the resource
     */
    display?: string
  }

  /**
   * The RelatedArtifact structure defines resources related to a module such as previous and next versions of documents, documentation, citations, etc.
   */
  interface RelatedArtifact extends Element {
    /**
     * documentation | justification | citation | predecessor | successor | derived-from | depends-on | composed-of 
     * (http://hl7.org/fhir/R4/valueset-related-artifact-type.html)
     */
    type: 'documentation' | 'justification' | 'citation' | 'predecessor' | 'successor' | 'derived-from' | 'depends-on' | 'composed-of'
    /**
     * Short label
     */
    label?: string
    /**
     * Brief description of the related artifact
     */
    display?: string
    /**
     * Bibliographic citation for the artifact
     */
    citation?: markdown
    /**
     * Where the artifact can be accessed
     */
    url?: url 
    /**
     * What document is being referenced
     */
    document?: Attachment
    /**
     * What resource is being referenced
     */
    resource?: canonical
  }

  /**
   * Base Resource
   */
  interface Resource {
    /**
     * The type of the resource.
     */
    resourceType?: code
    /**
     * Logical id of this artifact
     */
    id?: id
    /**
     * Metadata about the resource
     */
    meta?: Meta
    /**
     * A set of rules under which this content was created
     */
    implicitRules?: uri
    /**
     * Language of the resource content
     */
    language?: code
  }

  /**
   * A series of measurements taken by a device
   */
  interface SampledData extends Element {
    /**
     * Zero value and units
     */
    origin: Quantity
    /**
     * Number of milliseconds between samples
     */
    period: decimal
    /**
     * Multiply data by this before adding to origin
     */
    factor?: decimal
    /**
     * Lower limit of detection
     */
    lowerLimit?: decimal
    /**
     * Upper limit of detection
     */
    upperLimit?: decimal
    /**
     * Number of sample points at each time point
     */
    dimensions: positiveInt
    /**
     * Decimal values with spaces, or "E" | "U" | "L"
     */
    data: string
  }

  /**
   * A digital Signature - XML DigSig, JWT, Graphical image of signature, etc.
   */
  interface Signature extends Element {
    /**
     * Indication of the reason the entity signed the object(s)
     */
    type: Coding[]
    /**
     * When the signature was created
     */
    when: instant
    /**
     * Who signed
     */
    who?: Reference
    /**
     * The party represented
     */
    onBehalfOf?: Reference
    /**
     * The technical format of the signed resources
     */
    targetFormat: code
    /**
     * The technical format of the signature
     */
    sigFormat: code
    /**
     * The actual signature content (XML DigSig. JWT, picture, etc.)
     */
    data?: base64Binary
  }

  interface Task extends DomainResource {
    /**
     * Task Instance Identifier
     */
    identifier?: Identifier[]
    /**
     * Formal definition of task
     */
    instantiatesCanonical?: canonical
    /**
     * Formal definition of task
     */
    instantiatesUri?: uri
    /**
     * Request fulfilled by this task
     */
    basedOn?: Reference[]
    /**
     * Requisition or grouper id
     */
    groupIdentifier?: Identifier
    /**
     * Composite task
     */
    partOf?: Reference[]
    /**
     * draft | requested | received | accepted | rejected | ready | cancelled | in-progress | on-hold | failed | completed | entered-in-error 
     */
    status: TaskStatus
    /**
     * Reason for current status
     */
    statusReason?: CodeableConcept
    /**
     * E.g. "Specimen collected", "IV prepped"
     */
    businessStatus?: CodeableConcept
    /**
     * unknown | proposal | plan | order | original-order | reflex-order | filler-order | instance-order | option (http://hl7.org/fhir/valueset-task-intent.html)
     */
    intent: TaskIntent
    /**
     * routine | urgent | asap | stat (http://hl7.org/fhir/valueset-request-priority.html)
     */
    priority?: TaskUrgency
    /**
     * Task Type (Example: http://hl7.org/fhir/valueset-task-code.html)
     */
    code?: CodeableConcept
    /**
     * Human-readable explanation of task
     */
    description?: string
    /**
     * What task is acting on
     */
    focus?: Reference
    /**
     * Beneficiary of the Task
     */
    for?: Reference
    /**
     * Healthcare event during which this task originated (Must reference Encounter resource)
     */
    encounter?: Reference
    /**
     * Start and end time of execution
     */
    executionPeriod?: Period
    /**
     * Task Creation Date
     */
    authoredOn?: dateTime
    /**
     * Task Last Modified Date
     */
    lastModified?: dateTime
    /**
     * Who is asking for task to be done
     */
    requester?: Reference
    /**
     * Requested performer (http://hl7.org/fhir/valueset-performer-role.html)
     */
    performerType?: CodeableConcept[]
    /**
     * Responsible individual
     */
    owner?: Reference
    /**
     * Where task occurs
     */
    location?: Reference
    /**
     * Why task is needed as a CodeableConcept
     */
    reasonCode?: CodeableConcept
    /**
     * Why task is needed as a Reference
     */
    reasonReference?: Reference
    /**
     * Associated insurance coverage
     */
    insurance?: Reference[]
    /**
     * Comments made about the task
     */
    note?: Annotation[]
    /**
     * Key events in history of the Task
     */
    relevantHistory?: Reference[]
    /**
     * Constraints on fulfillment tasks
     */
    restriction?: TaskRestriction
    /**
     * Information used to perform task
     */
    input?: TaskInput[]
    /**
     * Information produced as part of task
     */
    output?: TaskOutput[]
  }

  /**
   * All possible values for Task.status
   */
  type TaskStatus = 'draft' | 'requested' | 'received' | 'accepted' | 'rejected' | 'ready' | 'cancelled' | 'in-progress' | 'on-hold' | 'failed' | 'completed' | 'entered-in-error' 

  /**
   * All possible values for Task.intent
   */
  type TaskIntent = 'unknown' | 'proposal' | 'plan' | 'order' | 'original-order' | 'reflex-order' | 'filler-order' | 'instance-order' | 'option'

  /**
   * All possible values for Task.urgency
   */
  type TaskUrgency = 'routine' | 'urgent' | 'asap' | 'stat'

  /**
   * Information used to perform the task
   */
  interface TaskInput extends BackboneElement {
    /**
     * Label for the input
     */
    type: CodeableConcept
    /**
     * Content to use in performing the task as a canonical reference
     */
    valueCanonical?: canonical
    /**
     * Content to use in performing the task as a Reference
     */
    valueReference?: Reference
  }

  /**
   * Information produced as part of a task
   */
  interface TaskOutput extends BackboneElement {
    /**
     * Label for output
     */
    type: CodeableConcept
    /**
     * Result of output as a canonical reference
     */
    valueCanonical?: canonical
    /**
     * Result of output as a Reference
     */
    valueReference?: Reference
  }

  interface TaskRestriction extends BackboneElement {
    /**
     * How many times to repeat
     */
    repetitions?: positiveInt
    /**
     * When fulfillment sought
     */
    period?: Period
    /**
     * For whom is fulfillment sought?
     */
    recipient?: Reference[]
  }

  /**
   * A timing schedule that specifies an event that may occur multiple times
   */
  interface Timing extends Element {
    /**
     * When the event occurs
     */
    event?: dateTime[]
    /**
     * When the event is to occur
     */
    repeat?: TimingRepeat
    /**
     * BID | TID | QID | AM | PM | QD | QOD | Q4H | Q6H +
     */
    code?: CodeableConcept
  }

  /**
   * When the event is to occur
   */
  interface TimingRepeat extends Element {
    /**
     * Length/Range of lengths, or (Start and/or end) limits
     */
    boundsDuration?: Duration
    /**
     * Length/Range of lengths, or (Start and/or end) limits
     */
    boundsRange?: Range
    /**
     * Length/Range of lengths, or (Start and/or end) limits
     */
    boundsPeriod?: Period
    /**
     * Number of times to repeat
     */
    count?: positiveInt
    /**
     * Maximum number of times to repeat
     */
    countMax?: positiveInt
    /**
     * How long when it happens
     */
    duration?: decimal
    /**
     * How long when it happens (Max)
     */
    durationMax?: decimal
    /**
     * s | min | h | d | wk | mo | a - unit of time (UCUM)
     */
    durationUnit?: 's' | 'min' | 'h' | 'd' | 'wk' | 'mo' | 'a'
    /**
     * Event occurs frequency times per period
     */
    frequency?: positiveInt
    /**
     * Event occurs up to frequencyMax times per period
     */
    frequencyMax?: positiveInt
    /**
     * Event occurs frequency times per period
     */
    period?: decimal
    /**
     * Upper limit of period (3-4 hours)
     */
    periodMax?: decimal
    /**
     * s | min | h | d | wk | mo | a - unit of time (UCUM)
     */
    periodUnit?: 's' | 'min' | 'h' | 'd' | 'wk' | 'mo' | 'a'
    /**
     * mon | tue | wed | thu | fri | sat | sun
     */
    dayOfWeek?: ('mon' | 'tue' | 'wed' | 'thu' | 'fri' | 'sat' | 'sun')[]
    /**
     * Time of day for action
     */
    timeOfDay?: time[]
    /**
     * Regular life events the event is tied to
     */
    when?: code[]
    /**
     * Minutes from event (before or after)
     */
    offset?: unsignedInt
  }

  /**
   * The TriggerDefinition structure defines when a knowledge artifact is expected to be evaluated
   */
  interface TriggerDefinition extends Element {
    /**
     * named-event | periodic | data-changed | data-added | data-modified | data-removed | data-accessed | data-access-ended
     * (http://hl7.org/fhir/R4/valueset-trigger-type.html)
     */
    type: 'named-event' | 'periodic' | 'data-changed' | 'data-added' | 'data-modified' | 'data-removed' | 'data-accessed' | 'data-access-ended'
    /**
     * Name or URI that identifies the event
     */
    name?: string
    /**
     * Timing of the event as Timing resource
     * Only one of timingTiming, timingReference, timingDate or timingDateTime are allowed simultaneously
     */
    timingTiming?: Timing
    /**
     * Timing of the event as Reference resource
     * Only one of timingTiming, timingReference, timingDate or timingDateTime are allowed simultaneously
     */
    timingReference?: Reference
    /**
     * Timing of the event as date
     * Only one of timingTiming, timingReference, timingDate or timingDateTime are allowed simultaneously
     */
    timingDate?: date
    /**
     * Timing of the event as dateTime
     * Only one of timingTiming, timingReference, timingDate or timingDateTime are allowed simultaneously
     */
    timingDateTime?: dateTime
    /**
     * Triggering data of the event (multiple = 'and')
     */
    data?: DataRequirement[]
    /**
     * Whether the event triggers (boolean expression)
     */
    condition?: Expression
  }

  interface UsageContext extends Element {
    /**
     * Type of context being specified
     */
    code: Coding
    /**
     * Value that defines the context in CodeableContext form
     */
    valueCodeableConcept?: CodeableConcept
    /**
     * Value that defines the context in Quantity form
     */
    valueQuantity?: Quantity
    /**
     * Value that defines the context in Range form
     */
    valueRange?: Range
    /**
     * Value that defines the context in Reference form
     */
    valueReference?: Reference
  }
}
